KIVY SAMPLES
============

>Samples from [kivy's project](http://kivy.org/) and other projects.

1 INSTALL in Debian Buster
--------------------------

In the following instructions, follow these guidelines:

* all the installations are based on `python3`
* when installing dependencies use `python3` wherever `python` is mentioned
* install dependencies _system-wide_, i.e. as `root`
* prefer, where possible, the installation through `pip3` compared to the one via `apt`. 

**Kivy**

>Follow instructions at _[Installation for Developement](https://github.com/kivy/kivy/blob/master/doc/sources/installation/installation-devel.rst)_.<br/>  

**p4a (python-for-android)**

>Follow these instructions:

>```
$ git clone https://github.com/kivy/python-for-android.git
$ cd python-for-android
$ python3 setup.py build
$ sudo python3 setup.py install
>```

**buildozer (python3)**

>Follow these instructions:

>```
$ git clone https://github.com/kivy/buildozer
$ cd buildozer
$ python3 setup.py build
$ sudo python3 setup.py install
>```

2 BUILD application for Android
-------------------------------

General instructions are at [Create a package for Android](https://kivy.org/doc/stable/guide/packaging-android.html).

**Android SDK**

>Follow instructions at [Installing Android SDK](https://github.com/kivy/python-for-android/blob/develop/doc/source/quickstart.rst) section and download these packages:

>>```
sdk-tools-linux-4333796.zip
android-ndk-r19c-linux-x86_64.zip
>>```

>Extract (install) the packages into a directory and setup environment into `.bashrc` file:

>```
#Android base path
ANDROIDPATH=<ANDROID_INSTALLATION_DIRECTORY>
>
# adb
export PATH=${PATH}:${ANDROIDPATH}/platform-tools:${ANDROIDPATH}/tools/bin:${ANDROIDPATH}/tools
>
# kivy/python-for-android
export ANDROIDSDK="${ANDROIDPATH}"
export ANDROIDNDK="${ANDROIDPATH}/android-ndk-r19c"
export ANDROIDAPI="27"  # Target API version of your application
export NDKAPI="21"  # Minimum supported API version of your application
export ANDROIDNDKVER="r19c"  # Version of the NDK you installed
>```

**Using p4a**

>Follow instructions at 
[Usage](https://github.com/kivy/python-for-android/blob/master/doc/source/quickstart.rst) section.

**Using buildozer**

>Install following instruction at 
[Quickstart](https://github.com/kivy/buildozer/blob/master/docs/source/quickstart.rst).

#!/bin/bash

p4a apk --private ${1} --package="${2}" --name "${3}" --version 0.1 \
		--bootstrap=sdl2 --requirements=python3,kivy
